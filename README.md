# Stalker Anomaly CPU Affinity

A small bash script that sets CPU affinity for [Stalker Anomaly](https://www.moddb.com/mods/stalker-anomaly). Also works for any modpacks based on it. Made specifically for Linux.

X-Ray Monolith engine is very CPU limited because it runs on just one core for most of it's functions, especially A-Life stuff. If you run a lot of other processes in the background while playing, your core 0 might choke and you'll get stutter. This script will force the Anomaly executable to unload core 0 and distribute the load to other cores.

**Performance without the script:**

![Performance without the script](img/perf_without.png)

**Performance with the script:**

![Performance with the script](img/perf_with.png)

## Installation

0. This assumes you already installed Anomaly (or a modpack for it) through Lutris.
1. Download `anomaly_cpu_affinity.sh` from this repo.
2. Make sure it's executable.
3. Move it somewhere appropriate, like the tools directory of Anomaly (~/Games/anomaly/drive_c/Anomaly/tools). Although the exact place you put it doesn't matter.
4. Select it as a Pre-launch script in Lutris.

![Lutris installation](img/installation.png)
