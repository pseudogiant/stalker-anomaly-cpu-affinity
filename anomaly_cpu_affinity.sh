#!/usr/bin/env bash

# Exit immediately if any command within exits with a non-zero status.
set -e

# Get the number of cores. Need to subtract 1 from the total number,
# since taskset expects that cores start indexing from 0.
AVAILABLE_CORES=$(($(nproc --all) - 1))

# Wait for Anomaly executable to start, then get it's pid.
until EXECUTABLE_PID=$(pgrep 'AnomalyDX*')
do
    sleep 1
done

# Once the game has started, have to wait a bit to get to the main menu,
# otherwise this doesn't work for some reason.
sleep 30 

# Offload core 0 by setting CPU affinity for Anomaly executable to core 1 
# through max number of cores available.
taskset -cp 1-$AVAILABLE_CORES $EXECUTABLE_PID
